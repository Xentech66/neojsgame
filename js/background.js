let background = {}, stars = [];

function loadBackgrounds(game) {
    game.load.image('star', 'assets/star.png');
    game.load.image('background1', 'assets/blackhole.png');
    game.load.image('background2', 'assets/blackhole2.png');
    game.load.image('background3', 'assets/nebula.png');
    game.load.image('background4', 'assets/nebula2.png');
    game.load.image('background5', 'assets/cube-earth.png');

}

function createBackground(game) {
    background.x = (win_width * Math.random()) ;
    background.y = (win_height * Math.random() ); // TODO Get image height
    let nbr = getRandomInt(5) + 1;
    background.image = game.add.image(background.x, background.y, 'background' + nbr);
    for(let i = 0;i < 50;i++)
        addStar(game,true);
}

function updateBackground(game) {
    background.image.rotation += background_rotation;
    if (getRandomInt(star_frequency) === 7) {
        addStar(game);
    }
    let move_x = ((player.x - base_x) / (win_width/background_move)) ;
    let move_y = ((player.y - base_y) / (win_height/background_move)) ;
    manageStars(move_x,move_y);
    background.image.setPosition(background.x + move_x,background.y + move_y);
}

function addStar(game,rand_x) {
    let star = {
        x: rand_x === undefined ? win_width : getRandomInt(win_width),
        y: getRandomInt(win_height),
        vel: getRandomInt(star_vel)+1,
        scale_dir: getRandomBool(0.5),
        image: game.add.image(0, 0, 'star'),
    };
    star.max_scale = (Math.random() * star.vel) + star_min_scale;
    star.scale = Math.random() * star.max_scale;
    star.image.setScale(star.scale,star.scale);
    stars.push(star);
}

function manageStars(move_x,move_y) {
    stars.forEach(function (star, index) {
        /** Manage position */
        star.x -= star.vel;
        star.image.setPosition(star.x - move_x*2, star.y - move_y*2);
        if (star.x < -13) {
            star.image.destroy();
            stars.splice(index, 1);
        }

        /** Manage scale */
        if(star.scale_dir){
            star.scale += star_scale_factor;
            if(star.scale > star.max_scale)
                star.scale_dir = !star.scale_dir;
        }  else {
            star.scale -= star_scale_factor;
            if(star.scale < star_min_scale)
                star.scale_dir = !star.scale_dir;
        }
        star.image.setScale(star.scale,star.scale);
    });
}