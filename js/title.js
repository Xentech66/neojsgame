let title,title_particles;

function loadTitle(game){
    game.load.image('title', 'assets/title.png');
    game.load.image('blue', 'assets/blue.png');
}

function createTitle(game) {
    title_particles = game.add.particles('blue');
    let emitter = title_particles.createEmitter({
        speed: 100,
        scale: { start: 1, end: 0 },
        blendMode: 'ADD',
        lifespan: 2000
    });

    title = game.physics.add.image(win_width / 2, win_height / 2, 'title');
    title.setScale(2);
    emitter.startFollow(title);
}

function removeTitle() {
    title.destroy();
    title_particles.destroy();
}
