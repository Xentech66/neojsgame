class Bullet extends Phaser.GameObjects.Sprite {
    constructor (scene, x, y,texture)
    {
        super(scene, x, y,texture);
        this.setScale(0.1,0.1);

        this.particle = scene.add.particles('plasma');
        let emitter = this.particle.createEmitter({
            speed: 100,
            scale: 0.1,
            blendMode: 'ADD',
            gravityX: 0,
            lifespan: 100,
        });
        emitter.setPosition(0, 0);
        emitter.startFollow(this);
        this.need_update = true;
    }

    preUpdate (time, delta)
    {
        super.preUpdate(time, delta);
        if(this.need_update){
            this.body.setSize(this.width,this.height);
            this.need_update = false;
        }
        this.x += bullet_vel;
        if(this.x > win_width)
            this.destroy();
    }

    destroy(){
        super.destroy();
        this.particle.destroy();
    }
}

let bullets;

function loadBullets(game) {
    game.load.image('bullet', 'assets/plasma.png');
}

function createBullets(game){
    bullets = game.physics.add.group();
}

function fire(game){
    bullets.add(new Bullet(game,player.x + 100, player.y + 10,'bullet'),true);
}