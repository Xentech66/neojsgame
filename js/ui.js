let life_icons;
let score_text;

function loadUi(game){
    game.load.image('life', 'assets/virus-logo.png');
}

function createUi(game) {
    score_text = game.add.text(10, 10, ui_text_score + score , { fontFamily: 'space', fontSize: 32, color: '#00ff00' });
    score_text.setVisible(false);
}

function launchUi(game) {
    life_icons = game.physics.add.group({
        key: 'life',
        repeat: lifes-1,
        setXY: {
            x: win_width - 200,
            y: 30,
            stepX: 50
        },
        setScale: {
            x: 0.05,
            y: 0.05
        }
    });
    score_text.setVisible(true);
}

function updateUi(game) {
    score_text.text = ui_text_score + score;
    if(life_icons.countActive() > lifes){
        life_icons.remove(life_icons.getFirst(true),true,true);
    }
}

function destroyUi() {
    score_text.destroy();
    life_icons.clear(true,true);
}