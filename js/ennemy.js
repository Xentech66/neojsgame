class Ennemy extends Phaser.GameObjects.Sprite {
    constructor (scene, x, y)
    {
        super(scene, x, y, getRandomBool(0.5) ? 'virus' : 'virus2');
        this.scale = Math.random() / 4 + 0.05;
        this.angular_vel = Math.random() / 15 + 0.001;
        this.direction = getRandomBool(0.5);

        this.setScale(this.scale,this.scale);
        this.need_update = true;
    }

    preUpdate (time, delta)
    {
        super.preUpdate(time, delta);
        if(this.need_update){
            this.body.setSize(this.width,this.height);
            this.need_update = false;
        }
        this.x -= (1/this.scale)/2;
        this.direction ? this.rotation += this.angular_vel : this.rotation -= this.angular_vel;
        if(this.x < -50){
            this.destroy();
            removeLife();
        }
    }


}

let ennemies;
let difficulty = 200;

function loadEnnemies(game) {
    game.load.image('virus', 'assets/virus.png');
    game.load.image('virus2', 'assets/virus2.png');

}

function createEnnemies(game){
    ennemies = game.physics.add.group();
    game.physics.add.overlap(player, ennemies , playerCollision, null, game);
    game.physics.add.overlap(bullets, ennemies , bulletCollision, null, game);

}

function updateEnnemies(game) {
    if(getRandomInt(difficulty) === 7)
        addEnnemy(game)
}

function addEnnemy(game){
    ennemies.add(new Ennemy(game,win_width,(Math.random() * win_height)),true);
}

function playerCollision(player,ennemy){
    removeLife();
    ennemy.destroy();
    ennemies.remove(ennemy);
}

function bulletCollision(bullet, ennemy) {
    bullet.destroy();
    bullets.remove(bullet);
    ennemy.destroy();
    ennemies.remove(ennemy);
    score += 1;
    difficulty -= 1;
}

function removeLife() {
    lifes -= 1;
    if(lifes === 0) {
        game_state = states.gameover;
        displayGameOver();
    }
}