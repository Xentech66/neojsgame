const win_width = window.innerWidth;
const win_height = window.innerHeight;
const speed_factor = 10;
const background_rotation = 0.001;
const star_vel = 2;
const star_scale_factor = 0.01;
const star_frequency = 30; // The lower for more frequent
const base_x = 200;
const base_y = window.innerHeight/2;
const background_move = 100;
const star_min_scale = 0.2;
const bullet_vel = 10;
const ui_text_score = "Score : ";
const states = {
    title: 0,
    playing: 1,
    gameover: 2
};