function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function getRandomBool(proba) {
    return Math.random() >= proba;
}